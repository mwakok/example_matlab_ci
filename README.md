# testbench

[![pipeline status](https://gitlab.tudelft.nl/mauritskok/testbench/badges/main/pipeline.svg)](https://gitlab.tudelft.nl/mauritskok/testbench/-/commits/main)
[![coverage matlab](https://gitlab.tudelft.nl/mauritskok/testbench/badges/main/coverage.svg?job=test_matlab&key_text=coverage+MATLAB&key_width=110)](https://gitlab.tudelft.nl/mauritskok/testbench/-/pipelines)
[![coverage python](https://gitlab.tudelft.nl/mauritskok/testbench/badges/main/coverage.svg?job=test_python&key_text=coverage+Python&key_width=110)](https://gitlab.tudelft.nl/mauritskok/testbench/-/pipelines)
[![GitHub deployments](https://img.shields.io/github/deployments/mwakok/gitlab-docs/github-pages?label=documentation)](https://mwakok.github.io/gitlab-docs/index.html)
[![License](https://img.shields.io/badge/license-MIT-green)](https://gitlab.tudelft.nl/mauritskok/testbench/-/blob/main/LICENSE)


Project to test the functionality of GitLab

### Testing
- [x] Test environment for Python
- [x] Test environment for Matlab
- [x] Code coverage badge Matlab
- [x] Separate coverage badges for Python and Matlab code
- [x] Separate unit and integration/regression testing 

### Documentation
- [x] Sphinx framework for .rst and .md
- [x] API references for Python code
- [x] API references for Matlab code
- [ ] ~CI job for deploying documentation in readthedocs~
- [x] Push sphinx build to Github for hosting with Pages

### CI jobs
- [x] Set up Gitlab runner on VPS (testbed.dcc.tudelft.nl)
- [x] Create Matlab docker image with license
- [x] Add CI job for packaging Matlab code
- [ ] ~Add CI job for publishing code to PyPI sandbox~
- [x] Create separate badges for each CI job (testing, documentation)
- [x] Stage and link CI jobs

