function compiler
% P-code compiler
%
% Create exact copy of matlab source code and replace all matlab files with
% pcode

here = pwd;
src_matlab = fullfile(here, 'src_matlab');

tmp = fullfile(here, 'pcode');
if ~isfolder(tmp)
    mkdir(tmp)
end
copyfile(src_matlab ,tmp)

list = dir(fullfile(tmp, '**/*.m'));
for i = 1:length(list)
    file = fullfile(list(i).folder, list(i).name);
    pcode(file,'-inplace');
    delete(file);
end

end