def sum_numbers(a, b):
    """ Add two numbers with Python
    
    Parameters
    ----------
    a : float
        First number
    b : float
        Second numbers

    Returns
    -------
    float
    
    """
    return a + b


def number_squared(a):
    """_summary_

    Parameters
    ----------
    a : float
        Input number
    
    Returns
    -------
        squared number
    
    """
    return a ** 2

