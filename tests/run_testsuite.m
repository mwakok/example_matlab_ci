function result = run_testsuite
%RUNTESTS - Run all tests and produce coverage report
%
%
% Resources:
% https://nl.mathworks.com/help/matlab/ref/matlab.unittest.plugins.codecoverageplugin-class.html
%

import matlab.unittest.TestSuite
import matlab.unittest.TestRunner
import matlab.unittest.plugins.CodeCoveragePlugin
import matlab.unittest.plugins.codecoverage.CoverageReport
import matlab.unittest.plugins.codecoverage.CoberturaFormat

% Define folder structure
here = pwd;
idx = strfind(here, filesep);
root = here(1:idx(end)-1);

testFolder = strcat(root, filesep, 'tests');
srcFolder = strcat(root, filesep, 'src_matlab');
reportFolder = strcat(testFolder, filesep, 'Reports', filesep);

% Add source folder plus all subfolders to the path.
addpath(genpath(srcFolder));

% Create test suite
suite = TestSuite.fromFolder(testFolder, 'IncludingSubfolders', true);
runner = TestRunner.withTextOutput;
if ~exist(reportFolder, 'dir')
    mkdir(reportFolder)
end

% Setup files to test
dirOut = dir(fullfile(srcFolder, '**', '*.m'));
codeFilePaths = string({dirOut.folder}) + filesep + string({dirOut.name});
filePathsToExclude = {'run_testsuite.m'};
codeFilePaths(contains(codeFilePaths, filePathsToExclude)) = [];

% Prepare HTML format output
reportFileHTML = 'CoverageResults.html';
pluginHTML = CodeCoveragePlugin.forFile(codeFilePaths,...
    'Producing', CoverageReport(reportFolder, 'MainFile', reportFileHTML));
runner.addPlugin(pluginHTML);

% Prepare XML format output
reportFileXML = strcat(reportFolder, 'CoverageResults.xml');
reportFormat = CoberturaFormat(reportFileXML);
pluginXML = CodeCoveragePlugin.forFile(codeFilePaths,...
    'Producing', reportFormat);
runner.addPlugin(pluginXML);

result = runner.run(suite);
end
